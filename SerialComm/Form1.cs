﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Windows.Forms;

namespace SerialComm
{
    public partial class Form1 : Form
    {
        Action<string> MsgE, MsgI;

        public Form1()
        {
            InitializeComponent();
            this.Text = $"{Application.ProductName} ver {Application.ProductVersion}";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MsgE = (m) => { AddMsg(m, eMsgType.Error); };
            MsgI = (m) => { AddMsg(m, eMsgType.Info); };
            this.tbText.KeyDown += (s1, e1) => { if(e1.KeyCode == Keys.Enter) btSend.PerformClick(); };
        }

        enum eMsgType { Info, Error, Rx, Tx }
        delegate void AddMsgHandler(string m, eMsgType msgType);
        void AddMsg(string m, eMsgType msgType)
        {
            if (this.richTextBox1.InvokeRequired)
                this.richTextBox1.BeginInvoke(new AddMsgHandler(AddMsg), m, msgType);
            else
            {
                //문자에 시간정보 추가
                m = $"[{DateTime.Now.ToString("HH:mm:ss.fff")}] {m}";
                var textColor = Color.Black;
                if (msgType == eMsgType.Error) textColor = Color.Red;
                else if (msgType == eMsgType.Tx) textColor = Color.Tomato;
                else if (msgType == eMsgType.Rx) textColor = Color.Blue;

                var startpos = richTextBox1.SelectionStart;
                richTextBox1.AppendText($"{m}");
                richTextBox1.SelectionStart = startpos;
                richTextBox1.SelectionLength = m.Length;
                richTextBox1.SelectionColor = textColor;

                if (m.EndsWith("\n") == false) richTextBox1.AppendText("\n");
                richTextBox1.ScrollToCaret();
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            var bt = sender as Button;
            try
            {
                if (this.serialPort1.IsOpen)
                {
                    this.serialPort1.Close();
                }
                else
                {
                    this.serialPort1.PortName = this.tbPort.Text;
                    this.serialPort1.BaudRate = int.Parse(tbBaud.Text);
                    this.serialPort1.Open();
                }
                bt.Text = this.serialPort1.IsOpen ? "Close" : "Open";
                btSend.Enabled = this.serialPort1.IsOpen;
                MsgI($"Port Open : {this.serialPort1.IsOpen}");
            }
            catch (Exception ex)
            {
                MsgE("ERR:" + ex.Message);
            }
        }


        private void btSend_Click(object sender, EventArgs e)
        {
            if (this.serialPort1.IsOpen)
            {
                var msg = tbText.Text.Trim();
                if (string.IsNullOrEmpty(msg))
                {
                    MsgE("입력된 문자가 없습니다");
                    tbText.Focus();
                }
                else
                {
                    this.serialPort1.Write(msg);
                    AddMsg("<< " + msg, eMsgType.Tx);
                    this.tbText.Focus();
                    this.tbText.SelectAll();
                }

            }
            else MsgE("포트가 닫혀 있습니다");
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            var cnt = serialPort1.BytesToRead;
            var buffer = new byte[cnt];
            serialPort1.Read(buffer, 0, cnt);
            var data = ">> " + System.Text.Encoding.Default.GetString(buffer);
            AddMsg(data, eMsgType.Rx);
        }

    }
}
